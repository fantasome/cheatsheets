from collections import namedtuple
from enum import Enum
import pandas as pd


class Color(Enum):
    yellow = 0
    cyan = 1
    magenta = 2


Coordinates = namedtuple('Coordinates', ['x', 'y'])

df = pd.DataFrame({'Col1': ['AAA01', 'AAA02', 'AAA03', 'AAA03'],
                   'Var1': [1., 2., 3., 4.],
                   'Var2': [5., 6., 7., 8.],
                   'Var3': ['aa', 'aa', 'ab', 'bb'],
                   'Var4': [Color.cyan, Color.yellow, Color.magenta, Color.yellow],
                   'Var6': [Coordinates(0, 0), Coordinates(0, 0), Coordinates(-.3, .10), Coordinates(4, 5)]}
                  )

df.to_csv('my_data.csv', index=False)

# Load a dataframe from a csv file called 'my_data.csv' and parse its 'Var4' column as an enum
df = pd.read_csv('my_data.csv', converters={'Var4': lambda x: Color(int(x))})

# Load a dataframe from a csv file called 'my_data.csv' and parse its 'Var6' column as a  named tuple
df = pd.read_csv('my_data.csv', converters={'Var6': lambda x: Coordinates(*[float(i) for i in x.split(',')])})

